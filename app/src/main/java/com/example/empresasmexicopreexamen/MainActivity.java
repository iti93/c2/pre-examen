package com.example.empresasmexicopreexamen;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.empresasmexicopreexamen.Modelo.UsuariosDb;
import com.example.empresasmexicopreexamen.Modelo.UsuariosDbHelper;

public class MainActivity extends AppCompatActivity {

    private Button btnRegistrarse, btnSalir, btnLogin;
    private EditText txtNombre, txtContra,txtCorreo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarse();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnSalir = findViewById(R.id.btnSalir);
        btnRegistrarse = findViewById(R.id.btnRegistrarse);
        btnLogin = findViewById(R.id.btnLogin);
        txtNombre = findViewById(R.id.txtNombre);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtContra = findViewById(R.id.txtContra);
    }

    private boolean validarCampos() {
//        String nombre = txtNombre.getText().toString();
        String correo = txtCorreo.getText().toString();
        String contrasena = txtContra.getText().toString();
        return !correo.isEmpty() && !contrasena.isEmpty();
//        return !nombre.isEmpty() && !contrasena.isEmpty();
    }

    //Botón Registrar
    private void registrarse() {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
    }

    private void login() {
        String correo = txtCorreo.getText().toString();
        String contrasena = txtContra.getText().toString();

        if (validarCampos()) {
            UsuariosDb usuariosDb = new UsuariosDb(MainActivity.this);
            Usuario usuario = usuariosDb.getUsuario(correo);

            if (usuario != null) {
                if (usuario.getContrasena().equals(contrasena)) {
                    mostrarToast("Inicio de sesión exitoso");
                    Intent intent = new Intent(MainActivity.this, UserListActivity.class);
                    startActivity(intent);
                    // Finalizar la actividad actual
                    finish();
                } else {
                    mostrarToast("Correo o contraseña incorrectos");
                }
            } else {
                mostrarToast("Correo o contraseña incorrectos");
            }
        } else {
            mostrarToast("Por favor, ingrese todos los campos");
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("DOMINOS");
        confirmar.setMessage("¿Desea salir de la aplicación?");
        confirmar.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

}