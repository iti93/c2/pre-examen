package com.example.empresasmexicopreexamen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.empresasmexicopreexamen.Modelo.UsuariosDb;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {
    private ListView listViewUsuarios;
    private UsuariosAdapter usuariosAdapter;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        iniciarComponentes();

        listViewUsuarios = findViewById(R.id.listaUsuarios);

        // Obtener la lista de usuarios desde la base de datos
        UsuariosDb usuariosDb = new UsuariosDb(UserListActivity.this);
        ArrayList<Usuario> usuarios = usuariosDb.allUsuarios();

        // Crear y configurar el adaptador personalizado
        usuariosAdapter = new UsuariosAdapter(UserListActivity.this, usuarios);
        listViewUsuarios.setAdapter(usuariosAdapter);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });

    }

    private void iniciarComponentes() {
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Dominos");
        confirmar.setMessage("¿Desea salir de Dominos?");
        confirmar.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // Regresar al MainActivity
                Intent intent = new Intent(UserListActivity.this, MainActivity.class);
                startActivity(intent);
                finish(); // Finalizar la actividad actual
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }

}

