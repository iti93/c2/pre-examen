package com.example.empresasmexicopreexamen.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.empresasmexicopreexamen.Usuario;

import java.util.ArrayList;

public class UsuariosDb implements Persistencia, Proyeccion{

    private Context context;
    private UsuariosDbHelper helper;
    private SQLiteDatabase db;

    public UsuariosDb(Context context, UsuariosDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public UsuariosDb(Context context) {
        this.context = context;
        this.helper = new UsuariosDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertUsuario(Usuario usuario) {

        ContentValues values = new ContentValues();
        values.put(DefineTable.Usuarios.COLUMN_NAME_NOMBRE, usuario.getNombre());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTable.Usuarios.COLUMN_NAME_PASSWORD, usuario.getContrasena());

        this.openDataBase();
        long num = db.insert(DefineTable.Usuarios.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar", "insertUsuario: " + num);

        return num;
    }

    @Override
    public Usuario getUsuario(String correo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                DefineTable.Usuarios.COLUMN_NAME_CORREO + " = ?",
                new String[]{correo},
                null, null, null);

        Usuario usuario = null;
        if (cursor.moveToFirst()) {
            usuario = readUsuario(cursor);
        }

        cursor.close();

        return usuario;
    }



    @Override
    public ArrayList<Usuario> allUsuarios() {
        this.openDataBase(); // Abre la base de datos

        Cursor cursor = db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                null, null, null, null, null);
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Usuario usuario = readUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }

        cursor.close();

        this.closeDataBase();

        return usuarios;
    }

    @Override
    public Usuario readUsuario(Cursor cursor) {
        Usuario usuario = new Usuario();
        usuario.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DefineTable.Usuarios.COLUMN_NAME_ID)));
        usuario.setNombre(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Usuarios.COLUMN_NAME_NOMBRE)));
        usuario.setCorreo(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Usuarios.COLUMN_NAME_CORREO)));
        usuario.setContrasena(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Usuarios.COLUMN_NAME_PASSWORD)));
        return usuario;
    }
}
