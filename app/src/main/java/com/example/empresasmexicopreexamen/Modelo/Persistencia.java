package com.example.empresasmexicopreexamen.Modelo;

import com.example.empresasmexicopreexamen.Usuario;

public interface Persistencia {

     void openDataBase();
     void closeDataBase();
     long insertUsuario(Usuario usuario);
}
