package com.example.empresasmexicopreexamen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.empresasmexicopreexamen.Usuario;

import java.util.ArrayList;

public class UsuariosAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Usuario> usuarios;

    public UsuariosAdapter(Context context, ArrayList<Usuario> usuarios) {
        this.context = context;
        this.usuarios = usuarios;
    }

    @Override
    public int getCount() {
        return usuarios.size();
    }

    @Override
    public Object getItem(int position) {
        return usuarios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // Inflar el diseño del elemento de la lista
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_usuario, parent, false);

            // Crear un ViewHolder para almacenar las referencias de las vistas
            viewHolder = new ViewHolder();
            viewHolder.textViewNombre = convertView.findViewById(R.id.textViewNombre);
            viewHolder.textViewCorreo = convertView.findViewById(R.id.textViewCorreo);

            // Asignar el ViewHolder a la vista convertida
            convertView.setTag(viewHolder);
        } else {
            // Si convertView no es nulo, reutilizar el ViewHolder existente
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener el usuario en la posición actual
        Usuario usuario = usuarios.get(position);

        // Asignar los datos del usuario a las vistas correspondientes
        viewHolder.textViewNombre.setText(usuario.getNombre());
        viewHolder.textViewCorreo.setText(usuario.getCorreo());

        return convertView;
    }

    // Clase ViewHolder para almacenar las referencias de las vistas
    private static class ViewHolder {
        TextView textViewNombre;
        TextView textViewCorreo;
    }
}
